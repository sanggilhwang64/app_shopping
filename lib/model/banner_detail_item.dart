class BannerDetailItem {
  String bannerImageUrl;
  bool isUse;

  BannerDetailItem(this.bannerImageUrl, this.isUse);

  factory BannerDetailItem.fromJson(Map<String, dynamic> json) {
    return BannerDetailItem(
        json['bannerImageUrl'],
        json['isUse']
    );
  }
}