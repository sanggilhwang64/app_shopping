import 'package:app_shopping/model/banner_detail_item.dart';

class BannerListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<BannerDetailItem> list;

  BannerListResult(this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage, this.list);

  factory BannerListResult.fromJson(Map<String, dynamic> json) {
    return BannerListResult(
        json['isSuccess'] as bool,
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => BannerDetailItem.fromJson(e)).toList()
    );
  }
}
