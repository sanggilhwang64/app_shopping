import 'package:flutter/material.dart';

class ComponentGoodsItem extends StatelessWidget {
  const ComponentGoodsItem(
      {super.key, required this.imgUrl, required this.goodsName, required this.goodsPrice});

  final String imgUrl;
  final String goodsName;
  final double goodsPrice;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Image.network(
            imgUrl,
            width: 100,
            height: 100,
            fit: BoxFit.fill,
          ),
          Text(goodsName),
          Text(goodsPrice.toString())
        ],
      ),
    );
  }
}
