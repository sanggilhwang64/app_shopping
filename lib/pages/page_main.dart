import 'package:app_shopping/components/common/component_notification.dart';
import 'package:app_shopping/components/component_banner_item.dart';
import 'package:app_shopping/components/common/component_custom_loading.dart';
import 'package:app_shopping/components/component_goods_item.dart';
import 'package:app_shopping/model/banner_detail_item.dart';
import 'package:app_shopping/model/goods_detail_item.dart';
import 'package:app_shopping/repository/repo_banner.dart';
import 'package:app_shopping/repository/repo_goods.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class PageMain extends StatefulWidget {
  const PageMain({Key? key}) : super(key: key);

  @override
  State<PageMain> createState() => _PageMainState();
}

class _PageMainState extends State<PageMain> {
  List<BannerDetailItem> imgList = [];

  List<GoodsDetailItem> goodsListFood = [];

  Future<void> _getBannerList(String bannerImageUrl) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoBanner().getList(bannerImageUrl).then((res) {
      setState(() {
        imgList = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  Future<void> _getGoodsList(String goodsCategory) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoGoods().getList(goodsCategory).then((res) {
      setState(() {
        goodsListFood = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  @override
  void initState() {
    super.initState();
    _getGoodsList('FOOD');
    _getBannerList('bannerImageUrl');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('쇼핑 홈'),),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: CarouselSlider(
                options: CarouselOptions(
                  autoPlay: true,
                ),
                items: imgList.map((item) => ComponentBannerItem(imgUrl: item.bannerImageUrl)).toList(),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Text('IT'),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  for(int i = 0; i < goodsListFood.length; i++)
                    ComponentGoodsItem(
                      imgUrl: goodsListFood[i].goodsImageUrl,
                      goodsName: goodsListFood[i].goodsName,
                      goodsPrice: goodsListFood[i].totalPrice,
                    ),
                ],
              ),
            ),
            Text('식품'),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  for(int i = 0; i < goodsListFood.length; i++)
                    ComponentGoodsItem(
                        imgUrl: goodsListFood[i].goodsImageUrl,
                        goodsName: goodsListFood[i].goodsName,
                        goodsPrice: goodsListFood[i].totalPrice,
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
